package fullscreen;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LinesComponent extends JComponent {

private static class Line{
    final int x1; 
    final int y1;
    final int x2;
    final int y2;   
    final Color color;

    public Line(int x1, int y1, int x2, int y2, Color color) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.color = color;
    }               
}

private final LinkedList<Line> lines = new LinkedList<Line>();

public void addLine(int x1, int x2, int x3, int x4) {
    addLine(x1, x2, x3, x4, Color.black);
}

public void addLine(int x1, int x2, int x3, int x4, Color color) {
    lines.add(new Line(x1,x2,x3,x4, color));        
    repaint();
}

public void clearLines() {
    lines.clear();
    repaint();
}

@Override
protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    for (Line line : lines) {
        g.setColor(line.color);
        g.drawLine(line.x1, line.y1, line.x2, line.y2);
    }
}

public static void main(String[] args) {
    GraphicsEnvironment graphics =
    GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice device = graphics.getDefaultScreenDevice();
    Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    int width = (int)screenSize.getWidth();
    int height = (int)screenSize.getHeight();
    JFrame testFrame = new JFrame();
    device.setFullScreenWindow(testFrame);
    testFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    final LinesComponent comp = new LinesComponent();
    comp.setPreferredSize(new Dimension(width, height));
    testFrame.getContentPane().add(comp, BorderLayout.CENTER);
    testFrame.setUndecorated(true);
    testFrame.pack();
    testFrame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
    testFrame.setVisible(true);

    comp.addLine(width/3, 0, width/3, height, Color.BLACK);
    comp.addLine(width/3+1, 0, width/3+1, height, Color.BLACK);
    comp.addLine(width/3+2, 0, width/3+2, height, Color.BLACK);
    comp.addLine(width/3+3, 0, width/3+3, height, Color.BLACK);

    comp.addLine(2*width/3, 0, 2*width/3, height, Color.BLACK);
    comp.addLine(2*width/3+1, 0, 2*width/3+1, height, Color.BLACK);
    comp.addLine(2*width/3+2, 0, 2*width/3+2, height, Color.BLACK);
    comp.addLine(2*width/3+3, 0, 2*width/3+3, height, Color.BLACK);

    comp.addLine(0, height/3, width, height/3, Color.BLACK);
    comp.addLine(0, height/3+1, width, height/3+1, Color.BLACK);
    comp.addLine(0, height/3+2, width, height/3+2, Color.BLACK);
    comp.addLine(0, height/3+3, width, height/3+3, Color.BLACK);

    comp.addLine(0, 2*height/3, width, 2*height/3, Color.BLACK);
    comp.addLine(0, 2*height/3+1, width, 2*height/3+1, Color.BLACK);
    comp.addLine(0, 2*height/3+2, width, 2*height/3+2, Color.BLACK);
    comp.addLine(0, 2*height/3+3, width, 2*height/3+3, Color.BLACK);
    JPanel buttonsPanel = new JPanel();
    JButton newLineButton = new JButton("Quit");
    //JButton clearButton = new JButton("Clear");
    buttonsPanel.add(newLineButton);
    //buttonsPanel.add(clearButton);
    testFrame.getContentPane().add(buttonsPanel, BorderLayout.SOUTH);
    newLineButton.addActionListener(new ActionListener() {
    
        @Override
        public void actionPerformed(ActionEvent e) {
            testFrame.dispatchEvent(new WindowEvent(testFrame, WindowEvent.WINDOW_CLOSING));
        }
    }
    );
    testFrame.getContentPane().add(new MainClass());
    //clearButton.addActionListener(new ActionListener() {
    //
    //    @Override
    //    public void actionPerformed(ActionEvent e) {
    //        comp.clearLines();
    //    }
    //});
}

}
