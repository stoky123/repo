package fullscreen;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainClass extends JPanel {

  public MainClass() {

      addMouseListener(new MouseAdapter() { 
          public void mousePressed(MouseEvent me) { 
            int x=me.getX();
            int y=me.getY();

            System.out.println(x);
            System.out.println(y);
          } 
        }); 

  }

  //public static void main(String[] args) {
  //  JFrame frame = new JFrame();
  //  frame.getContentPane().add(new MainClass());
//
  //  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//
  //  frame.setSize(200, 200);
  //  frame.setVisible(true);
  //}
}