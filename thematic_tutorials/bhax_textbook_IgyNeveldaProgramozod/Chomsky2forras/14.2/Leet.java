import java.util.Scanner;
import java.util.HashMap;
import java.util.regex.Matcher; 
import java.util.regex.Pattern;


public class Leet {
	
	private String toLeet(String str) {
		//Meghatározzuk a regexpet, hogy milyen karakterekből állhat a string
		//ezeket a karaktereket fogjuk leet-el átalakítani
		Pattern pattern = Pattern.compile("[^a-zA-Z0-9]");
		//ez a későbbiekben kellesz, hogy a betűket összeadjuk egy stringgé
		StringBuilder sb = new StringBuilder();
		//Maga a map, itt párosítjuk össze a kulcsokat az értékekkel, vagyis
		//az eredeti karaktereket és a beillesztendő karaktereket
		HashMap<Character, String> map = new HashMap<Character, String>();
		
		//a map feltöltése...
		map.put('0', "()");
		map.put('1', "I");
		map.put('2',"z");
		map.put('3',"ε");
		map.put('4',"4");
		map.put('5',"s");
		map.put('6',"6");
		map.put('7',"7");
		map.put('8',"B");
		map.put('9',"g");
		map.put('A', "Д");
		map.put('B', "ß");
		map.put('C', "¢");
		map.put('D', "|)");
		map.put('E', "€");
		map.put('F', "|=");
		map.put('G', "6");
		map.put('H', "|-|");
		map.put('I', "!");
		map.put('J', "_7");
		map.put('K', "I<");
		map.put('L', "|_");
		map.put('M', "(V)");
		map.put('N', "И");
		map.put('O', "Ø");
		map.put('P', "|°");
		map.put('Q', "(,)");
		map.put('R', "Я");
		map.put('S', "$");
		map.put('T', "']['");
		map.put('U', "|_|");
		map.put('V', "|/");
		map.put('W', "Ш");
		map.put('X', "Ж");
		map.put('Y', "Ч");
		map.put('Z', "z");
		
		//végigmegyünk a paraméterként kapott string minden karakterén
		for (int i = 0; i < str.length(); i++) {
			char key = Character.toUpperCase(str.charAt(i));
			//megnézzük, hogy az adott karakter benne van-e a mapunkba
			Matcher matcher = pattern.matcher(Character.toString(key));
			//ha igen, akkor kicseréljük az értéket hozzáadjuk a stringünkhöz
			if (matcher.find()) {
				sb.append(key);
				sb.append(' ');
			} 
			//ha nincs akkor magát a kulcsot, tehát az eredeti karaktert fűzzük hozzá a stringhez
			else {
				sb.append(map.get(key));
				sb.append(' ');
			}
		}
		
		return sb.toString();
	}
	
	
	public static void main(String[] args) {
		//objektum létrehozása
		Leet obj = new Leet();
		Scanner scan = new Scanner(System.in);

		String leetWord;
		
		System.out.println("Press Ctrl+C to exit.");
		
		//folyamatosan kérjük be a szöveget, folyamatosan alakítjuk át a leeterrel és írjuk
		//ki a már átalakított szót később. Ezután új szót kér
		//Ha ki szeretnénk lépni a programból akkor nyomjunk Ctrl+C-t.
		while (true) {
			leetWord = scan.nextLine();
			
			String leet = obj.toLeet(leetWord);
			System.out.println("Leet: " + leet + "\n");
		}
	}
}
