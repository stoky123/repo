#ifndef POLARGEN_H
#define POLARGEN_H

#include <cstdlib>
#include <cmath>
#include <ctime>
using namespace std;

class PolarGen
{
public:
  PolarGen()
   {
	nincsTarolt=true;
	srand(time(NULL));
   }
  ~PolarGen()
   {
   }
   double kovetkezo ();
  
private:
   bool nincsTarolt;
   double tarolt;
};

#endif
