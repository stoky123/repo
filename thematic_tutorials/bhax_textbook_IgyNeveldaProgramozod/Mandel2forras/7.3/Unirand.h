#ifndef UNIRAND_H
#define UNIRAND_H

class Unirand {

private:
	std::function<int()> random;

public:
	Unirand(long seed, int min, int max);
};

#endif
