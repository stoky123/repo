import tensorflow as tf

#Adatok tárolása, felvétele. "Beépített" dataset van, tehát nem kell letölteni semmit, csak így deklaráljuk
mnist = tf.keras.datasets.mnist

#Adatok betöltése train és teszt adatszerkezetbe.
#Beigazgatjuk a képeket hogy olyan kép legyen, amilyen bemenetet el tud fogadni a neurális háló.
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

#A modell, neurális háló elkészítése.
#Itt beépített modelleket használunk.
#A paraméterezés a következőképpen működik:
#Ismét előre definiált rétegek vannak, ezeket csak sorban fel kell sorolni
#Először az input réteget és ennek a méretét adjuk meg. 28x28 pixeles képeket kell a bemenetre adni.
#Általában ilyen kis méretű képekkel szoktak dolgozni, 
#objektumdetektálás is a 10-es nagyságrendben mozog.
#Következő réteg (Dense) a klasszikus neurális háló réteg, sűrű réteg.
#Paraméterként adjuk meg a neuronok számát, második paraméter az aktivációs függvényt adja meg.
#Sokféle aktivációs függvény létezik, mi a relut fogjuk használni.
#Következő rétegünk Dropout layer lesz.
#Ezek a rétegek azt csinálják, hogy bizonyos neuronokat "kidobnak" a layerből, vagy bizonyos
#neuronokat nem továbbít a következő layernek. Általában a túlillesztést akarjuk vele kiküszöbölni.
#Az utolsó ismét egy Dense réteg, ez tulajdonképpen már a kimenet.
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10)
])

#Innentől már nagyrészt csak pure python script lesz.
#megírjuk a predictiont majd ki is írjuk. Ez a neurális háló kimenete.
predictions = model(x_train[:1]).numpy()
predictions

#A neurális háló kimenetét osztályokká kell alakítanunk.
tf.nn.softmax(predictions).numpy()

#Loss függvény.
#ez a függvény is segít a tanulás során, ez is előre definiált függvény a kerasban.
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

#eredmény kiiratása
loss_fn(y_train[:1], predictions).numpy()

#Modell összeállítása.
#Első paraméter az optimalizáló, a súlyok és a neurális háló optimalizálása. Ez egy kezdő értéket is jelenthet
#Második paraméter a loss függvény, az imént paraméterezett loss függvényt adjuk meg
#Harmadik paraméter a metrika, itt pontosságot adjuk meg.
model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])

#Itt kezdődik a tanítás. Átadjuk az adatbázist és az epochok számát.
model.fit(x_train, y_train, epochs=5)

#Kiértékelés.
model.evaluate(x_test,  y_test, verbose=2)

#Probability Model elkészítése.
#Első paraméter a model, második a softmax 
probability_model = tf.keras.Sequential([
  model,
  tf.keras.layers.Softmax()
])

#kiiratás
probability_model(x_test[:5])
