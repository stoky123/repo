class Vehicle{
    Vehicle()
    {
        System.out.println("I am Vehicle");
    }
    void start()
    {
        System.out.println("Start Vehicle");
    }
}

class Car extends Vehicle{
    Car() {
        System.out.println("I am Car");
    }

    @Override
    void start() {
        System.out.println("Start Car");
    }
}

class Supercar extends Car{
    Supercar() {
        System.out.println("I am SuperCar");
    }

    @Override
    void start() {
        System.out.println("Start SuperCar");
    }
}

public class cars {


    public static void main(String[] args)
    {
        Vehicle firstVehicle = new Supercar();  // lefut a legmélyebb class: a Vehicle konstruktora,
                                                // aztán a Car konstruktora, majd végül a Supercar konstruktora.
                                                // Mivel inheritáljuk a subclassból a superclasst, ezért mindent
                                                // tudnia kell a subclassnak amit a superclassnak, tehát le kell azt is konstruálni.
        firstVehicle.start();   // lefut a Supercar start-ja, mivel Overridoltuk mindig a start-ot.
        System.out.println(firstVehicle instanceof Car); // a Supercar természetesen Car, hiszen abból inheritál
        Car secondVehicle = (Car) firstVehicle; // egy Car-t képzünk a Supercarból (mármint csak úgy kezeljük mintha az lenne)
        secondVehicle.start();  // lefut a Supercar start-ja, mivel Overridoltuk mindig a start-ot.
                                // ez nem fog változni akkor sem, ha a superclass metódusát hívjuk, a dinamikus kötés miatt
                                // a classunknak konkrétan nem áll módjában az eredeti metódust meghívni
        System.out.println(secondVehicle instanceof Supercar);  // ugyan átcastoltuk Car-á az objektumunkat, de az attól
                                                                // a memóriában még mindig Supercarként fog tovább élni.
                                                                // A castolás csak azon változtat, hogyan tekintünk rá az adott kontextusban.
        //Supercar thirdVehicle = new Vehicle();    // Ez már le se fordul, hiszen baromság. Természetesen nem tudunk úgy
                                                    // kezelni egy superclass osztályú objektumot mint egyik subclassja
        //thirdVehicle.start();


    }
}