#include <iostream>

class Szulo {
    public:

    void beszel(){
        std::cout << "Szülő vagyok!\n";
    }
};

class Gyermek : public Szulo {
    public:

    void beszel(){
        std::cout << "Gyerek vagyok!\n";
    }

    int kor(){
        return 10;
    }
};

int
main() {
    Szulo *szulo = new Szulo();
    Szulo *gyerek = new Gyermek();

    gyerek->beszel();
    szulo->beszel();
    //std::cout << gyerek->kor();

    return 0;
}