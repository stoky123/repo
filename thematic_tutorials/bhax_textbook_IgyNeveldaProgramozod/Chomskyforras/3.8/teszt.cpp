typedef int (*fuggveny) (int, int);
typedef int(*(*G) (int)) (int, int);

int* mutatotvissza(int* a){
    return a;
}

int egeszetado(int egy, int ketto)
{
    return egy+ketto;
}

int* mutatot_visszaado(int a)
{
    return &a;
}

fuggveny asd (int a)
{
    return egeszetado;
}

int
main()
{
    //egész
    int egesz = 0;

    //egészre mutató mutató
    int* mutato = &egesz;

    //egész referenciája
    int& referencia = egesz;

    //egészek tömbje
    int tomb[4] = {0, 1, 2, 3};

    //egészek tömbjének referenciája
    int (&tombreferencia)[4] = tomb;

    //egészre mutató mutatók tömbje
    int* mutatotomb[4];

    //egészre mutató, mutatót visszaadó függvényre mutató mutató
    int* egeszremutat = mutatotvissza(mutato);

    //egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény
    fuggveny fuggvenyremutato = egeszetado;

    //függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre
    G g = asd;

    return 0;
}
