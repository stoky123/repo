#include <iostream>

using namespace std;

int
main()
{
    int val;

    cout << "Please enter the value to convert it to unary." << endl;

    cin >> val;
    cin.clear();

    cout << "The value in unary: ";

    for(int i = 0; i < val; i++)
    {
        cout << '1';
    }

    cout << endl;
}