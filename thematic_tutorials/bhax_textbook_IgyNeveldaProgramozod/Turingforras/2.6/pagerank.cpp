#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>

void kiir(const std::vector<double> &tomb){
    for_each(tomb.begin(), tomb.end(), [](double &pagerank) {std::cout << pagerank << '\n';});
}

double tavolsag(std::vector<double> &PR, std::vector<double> &PRv, const int size){
    double osszeg = 0.0;
    for (int i = 0; i < size; ++i)
        osszeg += (PRv[i] - PR[i]) * (PRv[i] - PR[i]);

    return sqrt(osszeg);
}

int main()
{
    int i, j;
    std::vector<std::vector<double>> T = {
    {0.0, 0.0, 1.0 / 3.0, 0.0},
    {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
    {0.0, 1.0 / 2.0, 0.0, 0.0},
    {0.0, 0.0, 1.0 / 3.0, 0.0}
    };

    std::vector<double> PR = {0.0, 0.0, 0.0, 0.0};
    std::vector<double> PR_elozo = {1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0};

    for(;;)
    {
        for(i = 0; i < PR.size(); i++)
        {
            PR[i] = 0.0;
            for(j = 0; j < PR.size(); j++)
                PR[i] += (T[i][j] * PR_elozo[j]);
        }
        if (tavolsag(PR, PR_elozo, PR_elozo.size()) < 0.00000001)
            break;
        for(i = 0; i < PR_elozo.size(); i++)
            PR_elozo[i] = PR[i];      
    }

    kiir(PR);
    return 0;
}
