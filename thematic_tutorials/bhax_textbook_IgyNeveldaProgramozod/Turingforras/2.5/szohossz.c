#include <stdio.h>

int
main()
{
	int szam = 1;
	int bitszamlalo = 0;
	do
	{
		bitszamlalo++;
		printf("%d\n", szam);
	}
	while(szam<<=1);

	printf("A szó ezen a gépen %i bit hosszú.\n", bitszamlalo);

	return 0;
}
