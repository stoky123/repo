#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <curses.h>

using namespace std;

int x, y;
int xmove = 1, ymove = 1;
int labdaszelesseg = 1, labdamagassag = 1;

int
main()
{
	WINDOW *ablak;
	ablak=initscr();
	start_color();
	init_pair(1, COLOR_RED, COLOR_BLACK);
	init_pair(2, COLOR_GREEN, COLOR_BLACK);
	init_pair(3, COLOR_YELLOW, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(6, COLOR_CYAN, COLOR_BLACK);
	init_pair(7, COLOR_WHITE, COLOR_BLACK);
	int random;
	attron(COLOR_PAIR(1));
	int currentcolor = 1;
	system("setterm -cursor off");
	attron(A_BOLD | A_UNDERLINE);

	for(;;)
	{
		getmaxyx(ablak, y, x);
		mvprintw( labdamagassag, labdaszelesseg, "DVD");
		refresh();
		usleep(50000);
		clear();
		usleep(20000);

		labdaszelesseg += xmove;
		labdamagassag += ymove;
		if(labdaszelesseg >= x-3 || labdaszelesseg <= 0)
		{
			attroff(COLOR_PAIR(currentcolor));
			xmove *= -1;
			random = rand() % 8;
			if(random == currentcolor && random != 1)
			{
				random += 1;
			}
			else if(random == currentcolor)
			{
				random -= 1;
			}
			attron(COLOR_PAIR(random));
			currentcolor = random;
		}
		if(labdamagassag >= y-1 || labdamagassag <= 0)
		{
			attroff(COLOR_PAIR(currentcolor));
			ymove *= -1;
			random = rand() % 8;
			if(random == currentcolor && random != 1)
			{
				random += 1;
			}
			else if(random == currentcolor)
			{
				random -= 1;
			}
			attron(COLOR_PAIR(random));
			currentcolor = random;
		}
	}

	return 0;
}
