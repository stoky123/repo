#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <curses.h>

using namespace std;

int x, y;
int xmove = 1, ymove = 1;
int labdaszelesseg = 0, labdamagassag = 0;

int
main()
{
	WINDOW *ablak;
	ablak=initscr();

	for(;;)
	{
		getmaxyx(ablak, y, x);
		mvprintw( labdamagassag, labdaszelesseg, "o" );
		refresh();
		usleep(10000);
		clear();
		usleep(10000);

		labdaszelesseg += xmove;
		labdamagassag += ymove;
		for(;labdaszelesseg == x-1;)
        {
            xmove *= -1;
            break;
        }
        for(;labdaszelesseg == 0;)
        {
            xmove *= -1;
            break;
        }
        for(;labdamagassag == y-1;)
        {
            ymove *= -1;
            break;
        }
        for(;labdamagassag == 0;)
        {
            ymove *= -1;
            break;
        }
    }

	return 0;
}



