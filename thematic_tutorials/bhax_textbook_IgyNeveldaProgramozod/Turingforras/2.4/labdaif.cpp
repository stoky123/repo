#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <curses.h>

using namespace std;

int x, y;
int xmove = 1, ymove = 1;
int labdaszelesseg = 1, labdamagassag = 1;

int
main()
{
	WINDOW *ablak;
	ablak=initscr();

	for(;;)
	{
		getmaxyx(ablak, y, x);
		mvprintw( labdamagassag, labdaszelesseg, "o");
		refresh();
		usleep(10000);
		clear();
		usleep(10000);

		labdaszelesseg += xmove;
		labdamagassag += ymove;
		if(labdaszelesseg >= x-1 || labdaszelesseg <= 0)
			xmove *= -1;
		if(labdamagassag >= y-1 || labdamagassag <= 0)
			ymove *= -1;
	}

	return 0;
}



