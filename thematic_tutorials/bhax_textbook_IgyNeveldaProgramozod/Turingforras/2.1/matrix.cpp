#include <iostream>
#include <cstdlib>
#include <omp.h>
#include <unistd.h>

int
main()
{
    //system("setterm -cursor off");
    #pragma omp parallel
    {
	for(;;)
	{
 	    if(rand()%2 == 0)
		std::cout<<' ';
	    else
		std::cout << "\033[92m" << rand()%2 <<"\033[0m";
	    //sleep(0.005);
	}
    }

    return 0;
}
