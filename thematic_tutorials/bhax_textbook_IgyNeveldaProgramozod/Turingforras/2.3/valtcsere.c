#include <stdio.h>

int
main()
{
	int a = 5, b = 91;
	printf("Csere előtt: a = %d, b = %d \n", a, b);
	a += b;
	b = a - b;
	a -= b;
	printf("Csere után: a = %d, b = %d \n", a, b);
	
	return 0;
}
