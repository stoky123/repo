class XOREncryption 
{ 
    // A szöveg titkosítására és feltörésére ugyan az a function használható
    public static String titkositas_tores(String inputString) 
    { 
        
	char kulcs = 'P'; 
   
        String outputString = "";
  
        int len = inputString.length(); 
  
        // XOR titkosítás a string összes karakterére
        for (int i = 0; i < len; i++)  
        { 
            outputString = outputString +  
            Character.toString((char) (inputString.charAt(i) ^ kulcs)); 
        } 
  
        System.out.println(outputString); 
        return outputString; 
    } 
  
    public static void main(String[] args)
    { 
        String sampleString = "A Java általános célú, objektumorientált programozási nyelv, amelyet a Sun Microsystems fejlesztett a ’90-es évek elejétől kezdve egészen 2009-ig, amikor a céget felvásárolta az Oracle. 2011-ben a Java 1.7-es verzióját az új tulajdonos gondozásában adták ki. A Java alkalmazásokat jellemzően bájtkód formátumra alakítják, de közvetlenül natív (gépi) kód is készíthető Java forráskódból. A bájtkód futtatása a Java virtuális géppel történik, ami vagy interpretálja a bájtkódot, vagy natív gépi kódot készít belőle, és azt futtatja az adott operációs rendszeren. Létezik közvetlenül Java bájtkódot futtató hardver is, az úgynevezett Java processzor. A Java nyelv a szintaxisát főleg a C és a C++ nyelvektől örökölte, viszont sokkal egyszerűbb objektummodellel rendelkezik, mint a C++. A JavaScript szintaxisa és neve hasonló ugyan a Java-hoz, de a két nyelv nem áll olyan szoros rokonságban, mint azt ezekből a hasonlóságokból gondolhatnánk. "; 
	
	String kulcs = "P";
	String tores = "";	
	
        System.out.println("Titkosított szöveg: "); 
        String titkosString = titkositas_tores(sampleString);

	System.out.println("Eredeti szöveg: ");
        titkositas_tores(titkosString); 
    } 
} 
