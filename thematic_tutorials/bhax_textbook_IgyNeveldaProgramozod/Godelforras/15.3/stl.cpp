#include <vector>
#include <iostream>
#include <map>
#include <bits/stdc++.h>

using namespace std;

vector<pair<string, int>> sortMap(map <string, int> rank)
{
	vector<pair<string, int>> sorted;
	for (auto& i : rank)
	{
		if (i.second)
		{
		pair<string, int> p{ i.first, i.second };
		sorted.push_back(p);
		}
	};
	sort(begin(sorted), end(sorted),[=](auto&& p1, auto&& p2) {
		return p1.second > p2.second;
	}
	);
	return sorted;
}

int main() {
    map<string, int> gamesMap;
    gamesMap.insert(make_pair("Red Dead Redemption 2", 10));
    gamesMap.insert(make_pair("Detroit Become Human", 5));
    gamesMap.insert(make_pair("Persona 5 Royal", 8));
    gamesMap.insert(make_pair("God of War", 3));
    gamesMap.insert(make_pair("The Witcher 3: Wild Hunt", 9));
    gamesMap.insert(make_pair("Uncharted 4: A Thief’s End", 2));
    gamesMap.insert(make_pair("Ghost of Tsushima", 7));
    gamesMap.insert(make_pair("The Last of Us Part II", 1));
    gamesMap.insert(make_pair("Spider-Man", 6));
    gamesMap.insert(make_pair("Death Stranding", 4));

    vector<pair<string, int>> sorted = sortMap(gamesMap);
    cout << "Rendezes utan:" << endl;
    for (auto& i : sorted)
        cout << i.first << " - " << i.second << endl;

    return 0;
}